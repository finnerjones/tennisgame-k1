# Tennis Game Kata

The solution presented here is based on the kata below

https://technologyconversations.com/2014/04/23/java-tutorial-through-katas-tennis-game-easy/

# Running the tests

The following commands will run the tests

```
$ gradle clean build
```

or

```
$ gradle clean test
```

or

```
$ gradle test
```