package com.finnerjones.tennisgame;

/**
 * Created by finner on 13/12/17.
 */
public class TennisGame {

    private int xavi = 0;
    private int jordi = 0;
    private static final String[] SCORES = {"Love", "Fifteen", "Thirty", "Forty", "Advantage", "Deuce"};


    public String getScore() {
        if (isGameOver()) {
            return printWinner();
        } else if (isAdvantage()) {
            return printAdvantage();
        } else if (isDeuce()) {
            return printDeuce();
        } else {
            return printRunningScore();
        }
    }

    public void jordiScores() {
        jordi++;
    }

    public void xaviScores() {
        xavi++;
    }

    private boolean isDeuce() {
        return (jordi >= 3) && (xavi >= 3) && (jordi == xavi) ;
    }

    private boolean isAdvantage() {
        return playerHasAtLeastThreePoints() && playerIsWinningByOnePoint();
    }

    private boolean isGameOver() {
        return playerIsWinningByTwoPoints() && playerHasAtLeastThreePoints();
    }

    private String printWinner() {
        if (jordi > xavi) {
            return "Winner is Jordi";
        } else {
            return "Winner is Xavi";
        }
    }

    private String printDeuce() {
        return SCORES[5];
    }

    private String printAdvantage() {
        if (jordi > xavi) {
            return SCORES[4] + " Jordi";
        } else {
            return SCORES[4] + " Xavi";
        }
    }

    private String printRunningScore() {
        return SCORES[jordi] + ", " + SCORES[xavi];
    }

    private boolean playerIsWinningByOnePoint() {
        return (jordi - xavi >= 1) || (xavi - jordi >= 1);
    }

    private boolean playerIsWinningByTwoPoints() {
        return (jordi - xavi >= 2) || (xavi - jordi >= 2);
    }

    private boolean playerHasAtLeastThreePoints() {
        return (jordi >= 4) || (xavi >= 4);
    }

}
