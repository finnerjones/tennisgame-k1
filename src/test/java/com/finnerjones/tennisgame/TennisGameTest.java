package com.finnerjones.tennisgame;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import org.junit.jupiter.api.Assertions;

/**
 * https://technologyconversations.com/2014/04/23/java-tutorial-through-katas-tennis-game-easy/
 * Created by finner on 13/12/17.
 */
public class TennisGameTest {

    public TennisGame game;

    @BeforeEach
    public void init() {
        game = new TennisGame();
        Assertions.assertNotNull(game);
    }

    @Test
    public void scoreIsLoveLove() {
        Assertions.assertEquals("Love, Love", game.getScore(),  "Expecting score to be Love, Love");
    }

    @Test
    public void scoreIsFifteenLove() {
        game.jordiScores();
        Assertions.assertEquals("Fifteen, Love", game.getScore(), "Expecting score to be Fifteen, Love");
    }

    @Test
    public void scoreIsThirtyFifteen() {
        game.jordiScores(); // Fifteen, Love
        game.jordiScores(); // Thirty, Love
        game.xaviScores();  // Thirty, Fifteen
        Assertions.assertEquals("Thirty, Fifteen", game.getScore(), "Expecting score to be Thirty, Fifteen");
    }

    @Test
    public void scoreIsFortyThirty() {
        game.jordiScores(); // Fifteen, Love
        game.jordiScores(); // Thirty, Love
        game.jordiScores(); // Forty, Love
        game.xaviScores();  // Forty, Fifteen
        game.xaviScores();  // Forty, Thirty
        Assertions.assertEquals("Forty, Thirty", game.getScore(), "Expecting score to be Forty, Thirty");
    }

    @Test
    public void scoreIsAdvantage() {
        game.jordiScores(); // Fifteen, Love
        game.xaviScores();  // Fifteen, Fifteen
        game.jordiScores(); // Thirty, Fifteen
        game.xaviScores();  // Thirty, Thirty
        game.jordiScores(); // Forty, Thirty
        game.xaviScores();  // Forty, Forty (Deuce)
        game.jordiScores(); // Advantage, Forty
        Assertions.assertEquals("Advantage Jordi", game.getScore(), "Expecting score to be Advantage Jordi");
    }

    @Test
    public void scoreIsDeuce() {
        game.jordiScores(); // Fifteen, Love
        game.xaviScores();  // Fifteen, Fifteen
        game.jordiScores(); // Thirty, Fifteen
        game.xaviScores();  // Thirty, Thirty
        game.jordiScores(); // Forty, Thirty
        game.xaviScores();  // Forty, Forty (Deuce)
        Assertions.assertEquals("Deuce", game.getScore(), "Expecting score to be Deuce");
    }

    @Test
    public void scoreIsDeuceAgain() {
        game.jordiScores(); // Fifteen, Love
        game.xaviScores();  // Fifteen, Fifteen
        game.jordiScores(); // Thirty, Fifteen
        game.xaviScores();  // Thirty, Thirty
        game.jordiScores(); // Forty, Thirty
        game.xaviScores();  // Forty, Forty (Deuce)
        game.jordiScores(); // Advantage, Forty (Deuce)
        game.xaviScores();  // Deuce
        Assertions.assertEquals("Deuce", game.getScore(), "Expecting score to be Deuce");
    }

    @Test
    public void gameHasAWinner() {
        game.jordiScores(); // Fifteen, Love
        game.xaviScores();  // Fifteen, Fifteen
        game.jordiScores(); // Thirty, Fifteen
        game.xaviScores();  // Thirty, Thirty
        game.jordiScores(); // Forty, Thirty
        game.xaviScores();  // Forty, Forty (Deuce)
        game.jordiScores(); // Advantage, Forty
        game.jordiScores(); // Game, Forty
        Assertions.assertEquals("Winner is Jordi", game.getScore(), "Expecting score to be Winner is Jordi");
    }

    @Test
    public void playAGameOfTennis() {
        game.xaviScores();
        Assertions.assertEquals("Love, Fifteen", game.getScore(), "Expecting score to be Love, Fifteen");
        game.xaviScores();
        Assertions.assertEquals("Love, Thirty", game.getScore(), "Expecting score to be Love, Thirty");
        game.xaviScores();
        Assertions.assertEquals("Love, Forty", game.getScore(), "Expecting score to be Love, Forty");
        game.jordiScores();
        Assertions.assertEquals("Fifteen, Forty", game.getScore(), "Expecting score to be Fifteen, Forty");
        game.jordiScores();
        Assertions.assertEquals("Thirty, Forty", game.getScore(), "Expecting score to be Thirty, Forty");
        game.jordiScores();
        Assertions.assertEquals("Deuce", game.getScore(), "Expecting score to be Deuce");
        game.jordiScores();
        Assertions.assertEquals("Advantage Jordi", game.getScore(), "Expecting score to be Advantage Jordi");
        game.xaviScores();
        Assertions.assertEquals("Deuce", game.getScore(), "Expecting score to be Deuce");
        game.xaviScores();
        Assertions.assertEquals("Advantage Xavi", game.getScore(), "Expecting score to be Advantage Xavi");
        game.jordiScores();
        Assertions.assertEquals("Deuce", game.getScore(), "Expecting score to be Deuce");
        game.xaviScores();
        Assertions.assertEquals("Advantage Xavi", game.getScore(), "Expecting score to be Advantage Xavi");
        game.xaviScores();
        Assertions.assertEquals("Winner is Xavi", game.getScore(), "Expecting score to be Winner is Xavi");
    }
}
